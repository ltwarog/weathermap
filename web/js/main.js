var map;
var marker;

function loadInformation(lat, lng){
    var position = {'lat': lat, 'lng': lng};
    $.ajax({
        type: 'POST',
        url: $("#map").data('path'),
        data: position,
        beforeSend: function () {
            $('<div class="ajax-loader"></div>').appendTo($('body'));
        },
        complete: function () {
            $("body").find(".ajax-loader").remove();
        },
        success: function (data) {
            $("#wetherModal .modal-content").html(data);
            $('#wetherModal').modal('show');
        }
    });
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 52.20579657466298, lng: 21.037133804301106},
        zoom: 8
    });
    map.addListener('click', function(e) {
        var position = e.latLng;
        if(marker){
            marker.setPosition(position);
        }else{
            marker = new google.maps.Marker({
                position: position,
                map: map
            });
        }
        loadInformation(position.lat(), position.lng());
    });
}