<?php
namespace MapsBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Jasuwienas\DataTableBundle\Repository\Interfaces\DataTableListInterface;
use MapsBundle\Entity\Weather;

class WeatherRepository extends EntityRepository implements DataTableListInterface
{
    public function getDataTableColumns()
    {
        return array('id', 'name', 'description', 'addDate', 'clouds', 'temp','wind');
    }
    
    public function createDataTableQueryBuilder($alias){
        return $this->createQueryBuilder($alias);
    }

    public function getTemplatesPath(){
        return 'MapsBundle:History:';
    }
    
    public function getMaxTemp(){
        $query = $this->_em->createQueryBuilder()
            ->select('MAX(weather)')
            ->from(Weather::class, 'weather')
            ;
        return $query->getQuery()->getSingleScalarResult();
    }
    
    public function getMinTemp(){
        $query = $this->_em->createQueryBuilder()
            ->select('MIN(weather)')
            ->from(Weather::class, 'weather')
            ;
        return $query->getQuery()->getSingleScalarResult();
    }
    
    public function getAvgTemp(){
        $query = $this->_em->createQueryBuilder()
            ->select('AVG(weather)')
            ->from(Weather::class, 'weather')
            ;
        return $query->getQuery()->getSingleScalarResult();
    }
    
    public function getCommonName(){
        $query = $this->_em->createQueryBuilder()
            ->select('weather.name')
            
            ->from(Weather::class, 'weather')
            ->addSelect('(select COUNT(w) FROM '.Weather::class.' as w WHERE w.name = weather.name) as c')
            ->groupBy('weather')
            ->orderBy('c', 'DESC')
            ->setMaxResults(1)
            ;
        $element = $query->getQuery()->getSingleResult();
        
        if(isset($element)){
            return $element['name'];
        }
        
        return '';
    }
    
    public function getCount(){
        $query = $this->_em->createQueryBuilder()
            ->select('count(weather)')
            ->from(Weather::class, 'weather')
            ;
        return $query->getQuery()->getSingleScalarResult();
    }
}