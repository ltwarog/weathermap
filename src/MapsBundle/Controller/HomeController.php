<?php
namespace MapsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Dwr\OpenWeatherBundle\Utility\Converter;
use MapsBundle\Entity\Weather;

class HomeController extends Controller
{

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('MapsBundle:Home:index.html.twig', []);
    }
    /**
     * @Route("/load-information", name="load_information")
     * @Method({"POST"})
     */
    public function loadInformationAction(Request $request)
    {
        $lat = $request->get('lat', null);
        $lng = $request->get('lng', null);
        
        if(empty($lat) || empty($lng)){
            return $this->createNotFoundException('Błędne współrzędne');
        }
        
        $em = $this->getDoctrine()->getManager();
        
        $openWeather = $this->get('dwr_open_weather');
        $weather = $openWeather->setType('Weather')->getByGeographicCoordinates($lng, $lat);
        
        $entity = new Weather();
        $entity->setClouds($weather->clouds()['all']);
        $entity->setDescription($weather->description());
        $entity->setLat($lat);
        $entity->setLng($lng);
        $entity->setName($weather->cityName());
        $entity->setTemp(Converter::kelvinToCelsius($weather->temp()));
        $entity->setWind($weather->windSpeed());
        
        $em->persist($entity);
        $em->flush($entity);
        
        return $this->render('MapsBundle:Home:modal.html.twig', ['entity' =>$entity]);
    }
    
}
