<?php
namespace MapsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

use MapsBundle\Service\DataTableListHistoryService;
use MapsBundle\Repository\WeatherRepository;

class HistoryController extends Controller
{
    private $service;
    private $repository;
    
    public function __construct(DataTableListHistoryService $service, WeatherRepository $repository) {
        $this->service = $service;
        $this->repository = $repository;
    }
    
    /**
     * @Route("/history", name="history")
     */
    public function indexAction(Request $request)
    {
        return $this->render('MapsBundle:History:index.html.twig', [
            'columns' => $this->service->getColumns(),
            'maxTemp' => $this->repository->getMaxTemp(),
            'minTemp' => $this->repository->getMinTemp(),
            'avgTemp' => $this->repository->getAvgTemp(),
            'count' => $this->repository->getCount(),
            'commonName' => $this->repository->getCommonName(),
        ]);
    }
    
    /**
     * @Route("/history-list", name="history_list")
     */
    public function apiListJsonAction(Request $request)
    {
        return new JsonResponse($this->service->get($request->query));
    }
   
}
