<?php

namespace MapsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="weather")
 * @ORM\Entity(repositoryClass="MapsBundle\Repository\WeatherRepository")
 */
class Weather
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $clouds;
    
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $temp;
    
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $wind;
    
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $lat;
    
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $lng;
    
    /**
     * @var \DateTime $addDate
     * @ORM\Column(name="add_date", type="datetime", nullable=false)
     */
    protected $addDate;
    
    public function __construct()
    {
        $this->addDate = new \DateTime();
    }
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    
    function getName() {
        return $this->name;
    }

    function getDescription() {
        return $this->description;
    }

    function getClouds() {
        return $this->clouds;
    }

    function getTemp() {
        return $this->temp;
    }

    function getWind() {
        return $this->wind;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    function setClouds($clouds) {
        $this->clouds = $clouds;
        return $this;
    }

    function setTemp($temp) {
        $this->temp = $temp;
        return $this;
    }

    function setWind($wind) {
        $this->wind = $wind;
        return $this;
    }
    
    function getLat() {
        return $this->lat;
    }

    function getLng() {
        return $this->lng;
    }

    function getAddDate(): \DateTime {
        return $this->addDate;
    }

    function setLat($lat) {
        $this->lat = $lat;
        return $this;
    }

    function setLng($lng) {
        $this->lng = $lng;
        return $this;
    }

    function setAddDate(\DateTime $addDate) {
        $this->addDate = $addDate;
        return $this;
    }


}
